#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" generate imagemap for log-line-1.png """

from math import log
from PIL import Image

import logging, sys


map_name = sys.argv[1] if len(sys.argv) > 1 else 'log-line'
orders_of_magnitude = 2         # x-axis spans two orders of magnitude
axis_width = 12                 # width of the axis in units
radius = 6                      # radius of the sensible spots in the map in pixel


margin_right = margin_left = 1  # margin in units
scale_factor = axis_width/orders_of_magnitude
im = Image.open(map_name + '.png')
im_width, im_height = im.size

logging.debug(f'Image size: {im_width} x {im_height}')

def print_map(name, areas):
    with open(map_name + '.map', 'w') as f:
        f.write(f'<map name="{name}">\n')
        for area in areas:
            f.write(f'  <area shape="circle" coords="{area}">\n')
        f.write('</map>\n')


xs = [1, 1.2, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 6, 7, 8, 9]
xs += list(map(lambda x: 10*x, xs)) + [100]

total_width = margin_right + margin_left + axis_width
unit = im_width/total_width
areas = [f'{int(unit + scale_factor*unit*log(x)/log(10))},{int(unit)},{radius}' 
         for x in sorted(xs)]

print_map(map_name, areas)
logging.info(f'Image map for {map_name} written to "{map_name}.map".')
