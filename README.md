 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)  
 [![pipeline status](https://gitlab.com/thk1/exam-declaration/badges/master/pipeline.svg)](https://gitlab.com/thk1/exam-declaration/-/commits/master)  
 [![downloads artifacts](https://img.shields.io/badge/download%20artifacts-zip-yellow)](https://gitlab.com/oer2/imagemap/-/jobs/artifacts/master/download?job=build-map)  
 [![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)  
 [![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)

# Logarithmische Skala für ILIAS Fragetyp Hotspot/Imagemap

## Fragetyp Hotspot

Im *open source learning management system*
[ILIAS](https://www.ilias.de/) können verschiedene Fragetypen in
Übungen und Prüfungen verwendet werden.

Beim Typ **Hotspot/Imagemap** bekommen die
Prüfungskandidaten ein Bild angezeigt und müssen als Antwort eine oder
mehrere Bereiche auf dem Bild anklicken. Diese Bereiche können
rechteckig, kreisförmig oder vieleckig sein.

## Bild

![logarithmic scale](images/screenshot.jpg)

In der Datei `log-line.tex` wird eine logarithmische Skala über zwei Zehnerpotenzen definiert.

Die Skala ist 12 Einheiten breit, auf der rechten und linken Seite
befindet sich jeweils ein Rand von einer Einheit.

Mit dem Befehl

```
pdflatex -shell-escape log-line
```

wird das Bild im [*portable document*](https://de.wikipedia.org/wiki/Portable_Document_Format)- (PDF)
und [*portable networks graphics*](https://de.wikipedia.org/wiki/Portable_Network_Graphics)-Format (PNG) erzeugt. 

Die Datei im PNG-Format kann in ILIAS eingelesen werden<!-- ([download](https://gitlab.com/oer2/imagemap/-/jobs/artifacts/master/raw/build-map/log-line.png?job=build-map))-->.

## Hotspots

Die sensiblen Bereich werden aus einer [*HTML image
map*](https://www.w3schools.com/html/html_images_imagemap.asp)
(`<map>`) gelesen. Diese Map wird von dem
[Python](https://python.org)-Skript `mkimagemap` erzeugt:

```
python mkimagemap.py
```

Die Datei `log-line.map` kann in ILIAS eingelesen werden<!-- ([download](https://gitlab.com/oer2/imagemap/-/jobs/artifacts/master/raw/build-map/log-line.map?job=build-map))-->.

## Docker image

Das Docker-Image [`eltanin/imagemap`](https://hub.docker.com/repository/docker/eltanin/imagemap)
enthält alle Werkzeuge zur Erzeugung der Bild- und Mapdateien aus einer LaTeX-Quelle.

## GitLab CI/CD

Bild- und Mapdatei werden bei jedem Commit des Master-Branches automatisch erzeugt ([download zip](https://gitlab.com/oer2/imagemap/-/jobs/artifacts/master/download?job=build-map)).
